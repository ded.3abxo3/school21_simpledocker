#include <stdio.h>
#include <stdlib.h>
#include <fcgi_stdio.h>

int main() {
	while (FCGI_Accept() >= 0) {
		printf("Content-type: text/html\r\n\r\n");
		printf("<html>\n<head>\n<title>Hello World!</title>\n</head>\n<body style=\"background:black; color:white;\">\n");
		printf("<h1>Hello, World!</h1>\n<hr>\n<i>by willumye / School21</i>\n");
		printf("<br><br><b>localhost/status page in IFRAME tag</b><br><iframe src=\"/status\"></iframe>");
		printf("</body>\n</html>\n");
	}
	return 0;
}
