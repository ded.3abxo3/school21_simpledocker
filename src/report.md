## Part 1. Готовый докер

>ВНИМАНИЕ! В этом задании я делал все на виртуальной машине без графической оболочки. В качестве ОС выбрана Ubuntu Server 22.04 LTS (w/o GNU)
- Тянем официальный докер образ nginx при помощи docker pull
<br><img src=images/part1/img01.png>
- Проверяем наличие образа
<br><img src=images/part1/img02.png>
- Запускаем контейнер nginx `флаг -d в фоновом режиме`
<br><img src=images/part1/img03.png>
- Смотрим список процессов docker
<br><img src=images/part1/img04.png>
- Командой `docker inspect -s ID_контейнера` получаем информацию о контейнере в JSON формате. Флаг -s - размер контейнера.
  <br>Исходя из JSON вывода команды docker inspect имеем:
  - Размер в байтах контейнера(SizeRootFs) + размер файловой системы контейнера(SizeRw).
  <br><img src=images/part1/img05.png>
  - Замапленые порты
  <br><img src=images/part1/img06.png>
  - Адрес контейнера
  <br><img src=images/part1/img07.png>
- Останавливаем контейнер `docker stop ID_контейнера`
<br><img src=images/part1/img08.png>
- Смотрим список процессов докер, убеждаемся, что контейнер остановлен
<br><img src=images/part1/img09.png>
- Запускаем контейнер nginx с замаплеными портами 80 (HTTP) и 443 (HTTPS)
<br><img src=images/part1/img10.png>
- Проверяем в браузере `lynx localhost:80` доступ к контейнеру через 80 порт
<br><img src=images/part1/img11.png>

>в качестве дополнительной тренировки мозгов настроим доступ к контейнеру на виртуальной машине из основной хост-сети 192.168.10.0/24
><br>для этого создадим в настройках ВМ дополнительный адаптер типа мост с привязкой к сетевой карте из хост-сети
><br>изменим наш нетплан для выхода в хост-сеть
<br><img src=images/part1/img12.png>
><br>и ву-а-ля. Доступ к контейнеру из хост-сети через графический барузер
<br><img src=images/part1/img13.png>
><br>Получилась вот такая сеть
<br><img src=images/network.png>
><br><b>ВНИМАНИЕ! В дальнейшем в задании вместо localhost будет использоваться доступ к nginx контейнеру на виртуальной машине по адресу 192.168.10.50</b>

- Пезезапуск контейнера. Проверка процессов докер
<br><img src=images/part1/img14.png>

## Part 2. Операции с контейнером

- Прочитать конфигурационный файл nginx.conf внутри докер контейнера через команду exec. Создать на локальной машине файл nginx.conf
<br><img src=images/part2/img01.png>
>Здесь мы видим, что в файле конфигурации отсутствует директива server, НО к этому файлу подключаются дополнительные файлы конфигов
<br>Смотрим список файлов по пути `/etc/nginx/conf.d/`
<br><img src=images/part2/img02.png>
><br>в данной директории находится всего 1 файл default.conf
<br>Считываем его на хост-машину
<br><img src=images/part2/img03.png>
<br>Редактируем с добавлением отдачи страницы статуса nginx
<br><img src=images/part2/img04.png>
<br>Записываем отредактированный файл в контейнер, перезапускаем контейнер, проверяем процессы докер
<br><img src=images/part2/img05.png>
- Проверить, что по адресу localhost:80/status (в нашем случае 192.168.10.50/status) отдается страничка со статусом сервера nginx
<br><img src=images/part2/img06.png>
>Не забываем, что для наглядности реализован мост из основной хост-сети на виртуальную машину, т.е. nginx контейнер из хост-сети видится по адресу 192.168.10.50:80
><br>Для того, чтобы ТОЧНО удостовериться, что localhost работает, вводим на виртуальной машине `lynx localhost:80/status`
<br><img src=images/part2/img07.png>
><br>Как видим, все в порядке.
><br>Упростим задачу и сделаем вывод страницы `/status` в html-тэге iframe на главной странице сервера nginx. 
>Для этого отредактируем в контейнере файл `/usr/share/nginx/html/index.html` c добавлением туда html-тэга 
>`iframe src="/status"`
><br>Проверка:
<br><img src=images/part2/img08.png>
><br>Как видим, все работает.
- Экспортируй контейнер в файл container.tar через команду export.
<br><img src=images/part2/img09.png>
>обратите внимание, в данном случае мы обратились к контейнеру по имени `charming_gauss`. Имя контейнера можно увидеть в конце вывода `docker ps`
- Останови контейнер.
<br><img src=images/part2/img10.png>
- Удали образ через docker rmi [image_id|repository], не удаляя перед этим контейнеры.
<br><img src=images/part2/img11.png>
<br><img src=images/part2/img12.png>
- Удали остановленный контейнер.
<br><img src=images/part2/img13.png>
- Импортируй контейнер обратно через команду import.
<br><img src=images/part2/img14.png>
- Запусти импортированный контейнер.
<br><img src=images/part2/img15.png>
>параметр -g 'daemon off;' обеспечивает запуск контейнера на переднем плане
- Проверь, что по адресу localhost:80/status (в нашем случае 192.168.10.50 раздел iframe) отдается страничка со статусом сервера nginx.
<br><img src=images/part2/img16.png>

## Part 3. Мини веб-сервер

- <a href="materials/part3/">ИСХОДНЫЕ ФАЙЛЫ ЗАДАНИЯ</a>

>ВНИМАНИЕ! Далее для упрощения работы с файлами контейнера перейдем прямо в его командную строку
><br>Все действия по созданию и компиляции cgi-сценариев будем производить прямо в контейнере, чтобы каждый раз не пользоваться `docker cp`
<br><img src=images/part3/img01.png>

- Напиши мини-сервер на C и FastCgi, который будет возвращать простейшую страничку с надписью Hello World!.
<br><img src=images/part3/img02.png>
>Не забываем при компиляции подключить библиотеку
<br><img src=images/part3/img03.png>
>при возникновении ошибки `fcgi_stdio.h not found` установим spawn-fcgi `apt-get install libfcgi-dev spawn-fcgi`

- Запусти написанный мини-сервер через spawn-fcgi на порту 8080.
<br><img src=images/part3/img04.png>
<br>Добавляем в конфигурацию nginx обработку fast cgi сценариев по адресу localhost на порту 8080
<br><img src=images/part3/img0401.png>
>Перезагрузка nginx в контейнере командой `sudo nginx -s reload` для обновления конфига
<br>Проверка
<br><img src=images/part3/img05.png>

- Напиши свой nginx.conf, который будет проксировать все запросы с 81 порта на 127.0.0.1:8080.
>Поскольку наш текущий контейнер запущен с замаплеными портами 80 и 443, для мапинга порта 81 нужно будет перезапустить контейнер с другими параметрами.
>Для этого нам понадобится выгрузить текущий контейнер в новый образ с помощью `docker commit old_container new_image` и запустить новый образ с помощью `docker run`, но уже с добавлением мапинга `-p 81:81`
<br><img src=images/part3/img06.png>
><br>Запускаем новый контейнер на основе нового образа с замапленым портом 81
<br><img src=images/part3/img07.png>
<br>Запускаем снова `spawn-fcgi -p 8080 -n [наш CGI-минисервер]` и меняем конфиг nginx для прослушивания 81 порта
<br><img src=images/part3/img08.png>

- Проверь, что в браузере по localhost:81 отдается написанная тобой страничка.
<br><img src=images/part3/img09.png>

## Part 4. Свой докер

- <a href="materials/part4/">ИСХОДНЫЕ ФАЙЛЫ ЗАДАНИЯ</a>

- Напиши свой докер-образ: <a href="materials/part4/Dockerfile">Dockerfile</a>
<br><img src=images/part4/img01.png>

- Собери написанный докер-образ через <a href="materials/part4/builddocker.sh">docker build</a> при этом указав имя и тег
<br><img src=images/part4/img02.png>

- Проверь через docker images, что все собралось корректно
<br><img src=images/part4/img03.png>

- Запусти (<a href="materials/part4/rundocker.sh">rundocker.sh</a>) собранный докер-образ с маппингом 81 порта на 80 на локальной машине и маппингом папки ./nginx внутрь контейнера по адресу, где лежат конфигурационные файлы nginx'а
<br><img src=images/part4/img04.png>

- Проверь, что по localhost:80 доступна страничка написанного мини сервера
<br><img src=images/part4/img05.png>

- Допиши в ./nginx/nginx.conf проксирование странички /status, по которой надо отдавать статус сервера nginx
<br><img src=images/part4/img06.png>

- Перезапускаем докер-контейнер через <a href="materials/part4/rundocker.sh">rundocker.sh</a>

- Проверь, что теперь по localhost:80/status отдается страничка со статусом nginx
<br><img src=images/part4/img07.png>

## Part 5. Dockle

- <a href="materials/part5/">ИСХОДНЫЕ ФАЙЛЫ ЗАДАНИЯ</a>

>При необходимости <a href="materials/part5/dockle.install">устанавливаем dockle</a> (установка для Ubuntu 22.04)

>Т.к. dockle начал ругаться на NGINX_GPGKEY, пришлось пересобрать сервер не на базе `nginx:latest`, а на базе `debian:latest` с отдельной установкой nginx. Новые Dockerfile, builddocker.sh и rundocker.sh лежат <a href="materials/part5/">здесь</a>

- Просканируй образ из предыдущего задания через dockle [image_id|repository].
<br><img src=images/part5/img01.png>
>Получена ошибка, указывающая на то, что работа с контейнером ведется от пользователя root, а это небезопасно. Создадим нового пользователя и раздадим ему права на нужные папки в контейнере. Для этого дописываем в <a href="materials/part5/Dockerfile">Dockerfile</a> раздел `# Создание пользователя`. Также создадим скрипт запуска <a href="materials/part5/start.sh">start.sh</a> и укажем его в качестве точки входа. Также немного видоизменим <a href="materials/part5/fcgi/hello.c">hello.c</a> и добавим вывод статуса в тэг iframe
<br><img src=images/part5/img0101.png>

- Исправь образ так, чтобы при проверке через dockle не было ошибок и предупреждений
<br><img src=images/part5/img02.png>

- Проверка
<br><img src=images/part5/img03.png>

## Part 6. Базовый Docker Compose

<b>Напиши файл docker-compose.yml, с помощью которого:</b>

- <a href="materials/part6/">ИСХОДНЫЕ ФАЙЛЫ ЗАДАНИЯ</a>

- Подними докер-контейнер (<a href="materials/part6/my_nginx/rundocker.sh">my_nginx/rundocker.sh</a>) из Части 5 (он должен работать в локальной сети, т.е. не нужно использовать инструкцию EXPOSE и мапить порты на локальную машину)
  - С помощью команды `docker inspect` выясняем IP-адрес нашего nginx-cgi сервера
  <br><img src=images/part6/img01.png>

- Подними докер-контейнер с nginx, который будет проксировать все запросы с 8080 порта на 81 порт первого контейнера.
  - Сперва пропишем <a href="materials/part6/nginx/nginx_conf/default.conf">default.conf</a> для пустого сервера nginx с прослушкой порта 8080 и проксированием запросов на 81 порт fcgi-сервера
  <br><img src=images/part6/img02.png>
  - Напишем <a href="materials/part6/nginx/Dockerfile">Dockerfile</a> для сборки контейнера nginx и соберем его с помощью <a href="materials/part6/nginx/builddocker.sh">builddocker.sh</a>

- Замапь 8080 порт второго контейнера на 80 порт локальной машины.
  - Запускаем простой nginx с мапингом портов (<a href="materials/part6/nginx/rundocker.sh">rundocker.sh</a>)
  <br><img src=images/part6/img03.png>
  - Проверяем
  <br><img src=images/part6/img04.png>

- Останови все запущенные контейнеры.
<br><img src=images/part6/img05.png>

- Собери и запусти проект с помощью команд docker-compose build и docker-compose up.
  - Меняем в <a href="materials/part6/nginx/nginx_conf/default.conf">default.conf</a> для стандартного nginx и указываем в качестве прокси имя контейнера в сети docker `my_nginx_server`
  <br><img src=images/part6/img0501.png>
  - Пишем файл <a href="materials/part6/docker-compose.yml">docker-compose.yml</a> для сборки контейнеров
  <br><img src=images/part6/img0502.png>
  - Собираем контейнеры `docker-compose build`
  <br><img src=images/part6/img06.png>
  - Запускаем контейнеры `docker-compose up`
  <br><img src=images/part6/img07.png>

- Проверь, что в браузере по localhost:80 отдается написанная тобой страничка, как и ранее.
<br><img src=images/part6/img08.png>

## НУ ВОТ И ВСЕ СОБСТВЕННО...